using System;

namespace myApp
{
  using static TextMatching;

  class Test
  {
      static void Case(string e, string r)
      {
        // Handle a test cases.
        System.Console.WriteLine("Expected: {0}", e);
        System.Console.WriteLine("Result: {0}", r);
        if (e.Equals(r))
        {
          System.Console.WriteLine("Pass");
        }
        else
        {
          System.Console.WriteLine("Failure");
        }
        System.Console.WriteLine();
      }

      static int Main(string[] args)
      {
          string text = "Polly put the kettle on, polly put the kettle on, polly put the kettle on we'll all have tea";

          // Test 1
          string subtext = "Polly";
          Case("1, 26, 51", TextMatching.ShowMatches(text, subtext));

          // Test 2
          subtext = "polly";
          Case("1, 26, 51", TextMatching.ShowMatches(text, subtext));

          // Test 3
          subtext = "ll";
          Case("3, 28, 53, 78, 82", TextMatching.ShowMatches(text, subtext));

          // Test 4
          subtext = "Ll";
          Case("3, 28, 53, 78, 82", TextMatching.ShowMatches(text, subtext));

          // Test 5
          subtext = "X";
          Case("<no matches>", TextMatching.ShowMatches(text, subtext));

          // Test 6
          subtext = "Polx";
          Case("<no matches>", TextMatching.ShowMatches(text, subtext));

          // Test 7
          text = "Polly put the kettle on, police put the kettle on, polppolly put the kettle on we'll all have tea";
          subtext = "Poll";
          Case("1, 56", TextMatching.ShowMatches(text, subtext));

          // Special Case 1
          text = "boooooooowooool";
          subtext = "oooo";
          Case("2, 3, 4, 5, 6, 11", TextMatching.ShowMatches(text, subtext));
          return 0;
      }
  }
}
