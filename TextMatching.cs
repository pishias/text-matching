using System;
using System.Collections;
using System.IO;

namespace myApp
{
  public class TextMatching
  {
      public static int Help()
      {
        System.Console.WriteLine("Please enter a two string arguments.");
        System.Console.WriteLine("Usage: TextMatching <text> <subtext>");
        System.Console.WriteLine("Note: <text> must be longer than <subtext>");
        System.Console.WriteLine();
        return 1;
      }

      public static string ShowMatches(string t, string s)
      {
        // Display the positions of the matches if any.
        ArrayList positions = FindMatches(t,s);
        if ( !(positions.Count == 0) )
        {
          string res = "";
          for(int i=0; i<positions.Count-1; i++)
          {
            res = res + ((int) positions[i] + 1) + ", ";
          }
          res = res + ((int) positions[positions.Count-1] + 1);
          return res;
        }
        else
        {
          return "<no matches>";
        }
      }

      public static ArrayList FindMatches(string t, string s)
      {
        // Finds the matches in the two strings and return the positions. [Requirement 2]
        ArrayList positions = new ArrayList();

        // The <text> must always be longer than the <subtext> string
        // otherwise there can be no match
        if (t.Length < s.Length) {
          Help();
          throw (new IOException("Error: <subtext> is longer than <text>"));
        }
        if (t.Length == 0) {
          Help();
          throw (new IOException("Error: <text> is empty"));
        }
        if (s.Length == 0) {
          Help();
          throw (new IOException("Error: <subtext> is empty"));
        }
        // Make sure both are compared as lowercase strings,
        // enabling case insensitive matching. [Requirement 4]
        string tl = t.ToLower();
        string sl = s.ToLower();
        int slen = sl.Length;
        // Avoid unescessary computation and possible out of range exceptions.
        int max = tl.Length-slen+1;
        int m=0;
        while (m < max)
        {
          // DEBUG: System.Console.WriteLine("{0}", t[m]);
          // Go through the <text> and find a character that is the same
          // as the initial character of the <subtext>.
          if (tl[m].Equals(sl[0]))
          {
            // Deals with the special case in [Special Case 1]
            // where substring are overlapping until the pattern is broken
            // no need to check words starting with the char if we know there
            // is a missmatch aleady from the previous iteration
            int pos = CheckSubstring(tl,sl,m,slen);
            if (pos == m)
            {
              positions.Add(m);
              m = m + 1;
            }
            else {
              m = m + pos;
            }
          }
          else
          {
            m = m + 1;
          }
        }
        return positions;
      }

      public static int CheckSubstring(string t, string s, int start, int length)
      {
        // Check if the rest of the characters are the same.
        // Return the start index if correct or the index of the mistake
        for (int i=1; i<length; i=i+1)
        {
          // As soon as there is a missmatch stop iterations.
          // Avoid unescessary computation.
          if (!s[i].Equals(t[start+i])) {
            return i;
          }
        }
        return start;
      }
  }
}
