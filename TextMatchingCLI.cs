using System;
using System.Collections;
using System.IO;

namespace myApp
{
  using static TextMatching;

  class TextMatchingCLI
  {
      static int Main(string[] args)
      {
          // Test if input arguments were supplied:
          if (args.Length == 0)
          {
              return TextMatching.Help();
          }

          // DEBUG: System.Console.WriteLine("The text is {0} and the subtext is {1}.", text, subtext);
          try
          {
            System.Console.WriteLine(TextMatching.ShowMatches(args[0], args[1]));
          }
          catch (IOException e)
          {
            System.Console.WriteLine(e.Message);
          }

          return 0;
      }
  }
}
