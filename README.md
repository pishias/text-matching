# Text Matching

### Requirements
Write an application that fulfils the following requirements:
1. Accepts two strings as input: one string is called "text" and the other is called "subtext" in this problem statement,
2. Matches the subtext against the text, outputting the character positions of the beginning of each match for the subtext within the text.
3. Allows multiple matches
4. Allows case insensitive matching

### CLI
```bash
# > ./app.sh
mcs -out:app.exe TextMatching.cs TextMatchingCLI.cs
mono app.exe <text> <subtext>
```

### Tests
```bash
# > ./tests.sh
mcs -out:tests.exe TextMatching.cs Test.cs
mono tests.exe
```

#### Installing Mono on Mac (using [Homebrew](https://brew.sh/))
```bash
brew install mono
```
